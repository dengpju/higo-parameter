package parameter

type Parameter struct {
	Name  string
	Value interface{}
}

func New(name string, value interface{}) *Parameter {
	return &Parameter{Name: name, Value: value}
}

type Parameters []*Parameter

func (this Parameters) Find(name string) interface{} {
	for _, p := range this {
		if p.Name == name {
			return p.Value
		}
	}
	return nil
}

func (this Parameters) String(name string) string {
	val := this.Find(name)
	if val != nil {
		return val.(string)
	}
	return ""
}

type Callable func(p *Parameter)

// 遍历
func (this Parameters) ForEach(callable Callable) {
	for _, parameter := range this {
		callable(parameter)
	}
}
