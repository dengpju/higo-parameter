package main

import (
	"fmt"
	"gitee.com/dengpju/higo-parameter/parameter"
)

func main()  {
	parameters:=make(parameter.Parameters, 0)
	parameters = append(parameters, parameter.New("t1", 1))
	parameters = append(parameters, parameter.New("t2", 2))
	parameters = append(parameters, parameter.New("t3", 3))
	parameters.ForEach(func(i *parameter.Parameter) {
		fmt.Println(i.Name, i.Value)
	})
	parameters.ForEach(func(p *parameter.Parameter) {
		
	})
}
